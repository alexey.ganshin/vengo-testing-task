alexey-test
===========

# Endpoints #

- ***/products/list*** - View the products list
- ***/products/create*** - Create a product

# Setup #

1. Fill the app/config/parameters.yml.
2. Run ```php bin/console doctrine:database:create```
3. Run ```php bin/console doctrine:migrations:migrate```
