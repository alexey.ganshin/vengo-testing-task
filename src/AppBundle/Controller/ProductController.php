<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use AppBundle\Form\ProductType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends Controller
{
    /**
     * @Route("/product/list", name="list")
     * @Method("GET")
     * @Template()
     */
    public function listAction(): array
    {
        return [
            'products' => $this->getDoctrine()->getManager()->getRepository(Product::class)->findAll(),
        ];
    }

    /**
     * @Route("/product/create", name="creationForm")
     * @Method("GET")
     * @Template()
     */
    public function creationFormAction(): array
    {
        $editProductForm = $this->createForm(ProductType::class);

        return [
            'create_product_form' => $editProductForm->createView(),
        ];
    }

    /**
     * @Route("/product/create", name="create")
     * @Method("POST")
     */
    public function createFormAction(Request $request)
    {
        $newProduct = new Product();

        $editProductForm = $this->createForm(ProductType::class, $newProduct);

        $whitelistedParams = [];

        if (!empty($request->get('name'))) {
            $whitelistedParams['name'] = $request->get('name');
        }

        if (!empty($request->get('price'))) {
            $whitelistedParams['price'] = $request->get('price');
        }

        $editProductForm->handleRequest($request);

        if ($editProductForm->isSubmitted() && $editProductForm->isValid()) {
            $newProduct = $editProductForm->getData();

            var_dump($newProduct);

            $em = $this->getDoctrine()->getManager();

            $em->persist($newProduct);
            $em->flush();
        }

        return $this->redirectToRoute('list');
    }

    /**
     * @Route("/product/list/{name}", name="edit-product")
     * @ParamConverter("product", class="AppBundle:Product", options={"name" = "name"})
     * @Method("PATCH")
     * @Template()
     */
    public function patchAction(Request $request, Product $product): JsonResponse
    {
        $editProductForm = $this->createForm(ProductType::class, $product);

        $whitelistedParams = [];

        if (!empty($request->get('price'))) {
            $whitelistedParams['price'] = $request->get('price');
        }

        $editProductForm->submit($whitelistedParams, false);

        if ($editProductForm->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($product);
            $em->flush();

            return new JsonResponse([], JsonResponse::HTTP_OK);
        }

        return new JsonResponse([], JsonResponse::HTTP_BAD_REQUEST);
    }
}
