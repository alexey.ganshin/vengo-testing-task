<?php

namespace AppBundle\Logger;

use AppBundle\Entity\Log;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class PriceChangeLogger
{
    /**
     * @var EntityManagerInterface $em
     */
    private $em;

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
    }

    public function logOldPrice(float $oldPrice) {
        $log = new Log();

        $log
            ->setDateTime(new DateTime())
            ->setOldPrice($oldPrice);

        $this->em->persist($log);
        $this->em->flush();
    }
}
