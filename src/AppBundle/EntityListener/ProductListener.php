<?php

namespace AppBundle\EntityListener;

use AppBundle\Entity\Product;
use AppBundle\Logger\PriceChangeLogger;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

class ProductListener
{
    /**
     * @var PriceChangeLogger $priceChangeLogger
     */
    private $priceChangeLogger;

    /**
     * @var float $oldPrice
     */
    private $oldPrice;

    public function __construct(PriceChangeLogger $priceChangeLogger)
    {
        $this->priceChangeLogger = $priceChangeLogger;
    }

    public function preUpdate(Product $product, PreUpdateEventArgs $args)
    {
        $this->oldPrice = $args->getOldValue('price');
    }

    public function postUpdate(Product $product, LifecycleEventArgs $args)
    {
        $this->priceChangeLogger->logOldPrice($this->oldPrice);
    }
}
