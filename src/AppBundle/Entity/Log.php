<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Product
 *
 * @ORM\Table(name="log")
 * @ORM\Entity()
 */
class Log
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="datetime", type="datetime")
     */
    private $datetime;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float")
     */
    private $oldprice;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get datetime
     *
     * @return DateTime
     */
    public function getDateTime()
    {
        return $this->datetime;
    }

    /**
     * Get datetime
     *
     * @param DateTime $datetime
     * @return Log
     */
    public function setDateTime(DateTime $datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getOldPrice()
    {
        return $this->oldprice;
    }

    /**
     * Set price
     *
     * @param float $oldprice
     * @return Log
     */
    public function setOldPrice(float $oldprice)
    {
        $this->oldprice = $oldprice;

        return $this;
    }
}
